<?php
include ("nodo.php");
class arbol{

var $raiz;

function __construct(){
	$this->raiz = NULL;
}

function insert($cadena, $identificador){
	$this->raiz = $this->insertar($cadena, $identificador, $this->raiz);
}

function insertar($cadena, $identificador, $nodo){
	if($nodo == NULL){
		$nodo = new  nodo($cadena, $identificador);
	}
	else if($identificador < $nodo->identificador){

		$nodo->izq = $this->insertar($cadena, $identificador, $nodo->izq);
		if($this->calcAltura($nodo->der) - $this->calcAltura($nodo->izq)==-2){
			if($identificador < $nodo->izq->identificador){
				$nodo = $this->rotIzquierda($nodo);
			}else{
				$nodo = $this->rotDobleIzquierda($nodo);
			}
		}
	}
	else if($identificador > $nodo->identificador){
		$nodo->der = $this->insertar($cadena, $identificador, $nodo->der);

		if($this->calcAltura($nodo->der) - $this->calcAltura($nodo->izq) == 2){
			if($identificador > $nodo->der->identificador){
				$nodo = $this->rotDerecha($nodo);
			}else{
				$nodo = $this->rotDobleDerecha($nodo);
			}
		}
	}
	$nodo->altura = $this->alturaGeneral($this->calcAltura($nodo->izq), $this->calcAltura($nodo->der))+1;
	return $nodo;
}

function rotDerecha($nodo){
	$aux = $nodo->der;
	$nodo->der = $aux->izq;
	$aux->izq = $nodo;

	$nodo->altura = $this->alturaGeneral($this->calcAltura($nodo->izq), $this->calcAltura($nodo->der))+1;
	$aux->altura = $this->alturaGeneral($this->calcAltura($aux->der), $nodo->altura)+1;
	return $aux;
}

function rotIzquierda($nodo){
        $aux = $nodo->izq;
        $nodo->izq = $aux->der;
        $aux->der = $nodo;

        $nodo->altuura = $this->alturaGeneral($this->calcAltura($nodo->izq), $this->calcAltura($nodo->der))+1;
        $aux->altura = $this->alturaGeneral($this->calcAltura($aux->izq), $nodo->altura)+1;
        return $aux;
}

function rotDobleIzquierda($nodo){
	$nodo->izq = $this->rotDerecha($nodo->izq);
	return $this->rotIzquierda($nodo);
}

function rotDobleDerecha($nodo){
	$nodo->der = $this->rotIzquierda($nodo->der);
	return $this->rotDerecha($nodo);
}

function alturaGeneral($izquierdo, $derecho){
	if($izquierdo > $derecho){
		return $izquierdo;
	}else{
		return $derecho;
	}
}

function calcAltura($nodo){
	if($nodo == NULL){
		return -1;
	}
	else{
		return $nodo->altura;
	}
}

function preOrden($nodo){
	if($nodo == NULL){
		return;
	}
	else{
		echo ($nodo->identificador . " " . $nodo->cadena . "\n");
		$this->preOrden($nodo->izq);
		$this->preOrden($nodo->der);
	}
}


function posOrden($nodo){
	if($nodo == NULL){
		return;
	}
	else{
		$this->posOrden($nodo->izq);
		$this->posOrden($nodo->der);
		echo ($nodo->identificador . " " . $nodo->cadena . "\n");
	}
}


function inOrden($nodo){
	if($nodo == NULL){
		return;
	}
	else{
		$this->inOrden($nodo->izq);
		echo ($nodo->identificador . " " . $nodo->cadena . "\n");
		$this->inOrden($nodo->der);
	}
}
}
?>
