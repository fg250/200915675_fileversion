<?php
include ("arbol.php");

$avl = new arbol();
$avl->insert("uno ", 1);
$avl->insert("dos",2);
$avl->insert("cuatro",4);
$avl->insert("tres",3);
$avl->insert("cinco ", 5);
$avl->insert("seis",6);
$avl->insert("ocho",8);
$avl->insert("siete",7);
$avl->insert("nueve", 9);
$avl->insert("once",11);
$avl->insert("diez",10);

echo ("recorrido preorden: \n");
$avl->preOrden($avl->raiz);

echo ("recorrido posorden: \n");
$avl->posOrden($avl->raiz);

echo ("recorrido inorden: \n");
$avl->inOrden($avl->raiz);

?>
