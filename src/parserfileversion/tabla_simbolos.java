/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parserfileversion;

import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author FG250
 */
public class tabla_simbolos {

    private simbolo primero;
    private simbolo ultimo;
    private int tamano;
    
    public tabla_simbolos(){
        this.primero = null;
        this.ultimo=null;
        this.tamano = 0;                
    }
    
    public boolean isEmpty(){
        return (this.primero == null);
    }
    
    public tabla_simbolos add(simbolo nuevo){
        if(isEmpty()){
            primero = nuevo;
            ultimo = nuevo;
            //nuevo.siguiente = nuevo;
        }
        else {            
            nuevo.siguiente = null;
            ultimo.siguiente = nuevo;
            ultimo =nuevo;
        }
        
        this.tamano++;
        return this;
    }
    
    public boolean simExist(String nom, String amb){
        boolean existe=false;
        if(tamano != 0){
            simbolo temp = primero;
            while(temp != null){                
                if(temp.nombre.equalsIgnoreCase(nom)){
                    existe = true;
                    break;
                }
                temp = temp.siguiente;                 
            }
        }
        return existe;
    }
    
    public simbolo getSim(String nom, String amb){
        simbolo r=null;
        if(tamano != 0){
            simbolo temp = primero;
            while(temp != null){                
                if(temp.nombre.equalsIgnoreCase(nom)){
                    r = temp;
                    break;
                }
                temp = temp.siguiente;                 
            }
        }
        return r;
    }
    
    public simbolo get(int i){
        simbolo ret;
        
        int c = -1;
        ret = primero;
        while(c < i){
            ret = ret.siguiente;
            c++;
        }
        
        return ret;        
    }
    
    public simbolo getfirst(){        
        return (this.primero);
    }
    
    public simbolo getLast(){
        return(this.ultimo);
    }
    
    public int getSize(){
        return this.tamano;
    }
        
    public void imprimir(){
        
        if(tamano != 0){
            simbolo temp = primero;
            String str ="";
//            System.out.println("|Nombre    |Tipo      |Tamaño|Posicion|Visibilidad|Ambito    |Rol       |Dimension |Parametros|Herencia  |");
            System.out.println("*******************************************\n");
            while(temp != null){
                str =       "Nombre:      " + temp.nombre+"\n";
                str = str + "Tipo:        " + temp.tipo +"\n";
                str = str + "Tamaño:      " + temp.tamano + "\n";
                str = str + "Posicion:    " + temp.posicion + "\n";
                str = str + "Visibilidad: " + temp.visibilidad + "\n";
                str = str + "Ambito:      " + temp.ambito + "\n";
                str = str + "Rol:         " + temp.rol + "\n";
                str = str + "Dimension:   " + temp.dimension + "\n";
                str = str + "Parametros:  " + temp.parametros + "\n";
                str = str + "Herencia:    " + temp.herencia + "\n";
//                System.out.println("|"+temp.nombre+"|"+temp.tipo+"|" + temp.tamano+"|"+ temp.posicion+"|"+ temp.visibilidad+"|"+ temp.ambito+"|"+ temp.rol+"|"+ temp.dimension+"|"+ temp.parametros+"|"+ temp.herencia +"|");
                System.out.println(str+"*******************************************\n");
                temp = temp.siguiente;                 
            }
            System.out.println("TAMANO DE LA TABLA DE SIMBOLOS: "+this.getSize());
        }
        
    }
}
