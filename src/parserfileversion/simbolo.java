/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parserfileversion;

/**
 *
 * @author FG250
 */
public class simbolo {
    public String nombre;
    public String tipo;
    public int tamano;
    public int posicion;
    public String visibilidad;
    public String ambito;
//    public String acceso;
    public String rol;
    public int dimension;
    public int parametros;
    public int herencia;
    public simbolo siguiente ;
    
    public simbolo(String n, String tipo, int tam, int p, String v, String a, String rol, int d, int pa, int h){
        this.nombre = n;
        this.tipo = tipo;
        this.tamano = tam;
        this.posicion = p;
        this.visibilidad = v;
        this.ambito = a;
        this.rol = rol;
        this.dimension = d;
        this.parametros = pa;
        this.herencia = h;
    }
}
