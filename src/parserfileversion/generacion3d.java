/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parserfileversion;

import compilador.nodo;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author FG250
 */
public class generacion3d {
    public tabla_simbolos tabla = new tabla_simbolos();
    public nodo raiz = new nodo();
    public String c3="";
    public static int temp=0;
    public static int etiqueta=0;
    
    public generacion3d(tabla_simbolos t, nodo r){
        this.tabla= t;
        this.raiz = r;
        this.iniciar_traduccion(this.raiz);
    }
    
    nodo tmp;
    public void iniciar_traduccion(nodo raiz){
        //CONDICION PARA QUE OBTENGA LA CLASE
        if(raiz.getEtiqueta().equalsIgnoreCase("CLASES")){ 
            if(!raiz.hijos.isEmpty()){
                int tamClase = 0;
                
                //APUNTANDO AL NODO QUE CONTIENE EL NOMBRE DE LA CLASE
                raiz = raiz.hijos.get(0);  
                String nom=raiz.getEtiqueta();
                System.out.println("NOMBRE DE LA CLASE: "+nom); 
                int herencia;
                
                //REVISANDO SI LA CLASE TIENE HERENCIA O NO
                if(raiz.hijos.get(1).getEtiqueta().equalsIgnoreCase("EXTENDS")){
                    herencia = 1;                    
                }else{
                    herencia = -1;
                }
                nodo temp = raiz.hijos.get(0);
                
                //DETECTANDO LAS VARIABLES GLOBALES DE LA CLASE
                for(int i =0; i< temp.hijos.size(); i++){   
                    String nomAt= temp.hijos.get(i).getEtiqueta(); 
                }
                
                
                //OBTENIENDO LAS FUNCIONES DE LA CLASE
                for(int j=1; j<raiz.hijos.size(); j++){ 
                    nodo aux = raiz.hijos.get(j);
                    this.c3 += "void "+nom+"\n{\n";
                    this.TraduccionMetodo(aux,aux.hijos.get(0).getEtiqueta());
                    this.c3 += "}\n \n";
                }             
                
            }else{// SI NO TIENE CLASES DEFINIDAS, ENTONCES ES EL ARCHIVO MAIN
                System.out.println("ESTE ES EL ARCHIVO MAIN");
                this.c3 += "void main\n{\n";
                this.TraduccionMetodo(tmp,"main");
                this.c3 += "}";
            }            
       }else if(raiz.getEtiqueta().equalsIgnoreCase("EXPRESIONES")){
           tmp = raiz;
       }else if(!raiz.hijos.isEmpty()){ //CONTINUA MOVIENDOSE ENTRE LOS NODOS
            for(int i =0; i< raiz.hijos.size(); i++){      
                iniciar_traduccion(raiz.hijos.get(i));
           }
        }
        
    }
    
    String globalEtiqV = "";
    String globalEtiqF = "";
    
    //METODO DONDE SE GENERARA EL CONTENIDO EN C3D DE LOS METODOS
    public String TraduccionMetodo(nodo raiz, String func){
        //String c3="";
         for (nodo r : raiz.hijos) {             
             String lbl = r.getEtiqueta();
             
                    //JOptionPane.showMessageDialog(null,"ENTRO A NODO: "+lbl);
                if(lbl.equalsIgnoreCase("var")){
                    //NO HARA NADA, PQ ES SOLO UNA DECLARACION DE VARIABLE
                    
                }else if(lbl.equalsIgnoreCase("=")){ //ES UNA ASIGNACION'
                    JOptionPane.showMessageDialog(null,"ENTRO ACA! :D");
                    if(r.hijos.get(0).getEtiqueta().equalsIgnoreCase("var")){
                        String v =r.hijos.get(0).hijos.get(0).getEtiqueta();
                        simbolo aux = this.tabla.getSim(v, func);
                        if(aux == null){
                            //System.out.println("error semantico");
                        }else{
                            String result="";
                            String t = this.getTemp();
                            String pos = "p + "+ aux.posicion;
                            c3 += t +" = "+pos + ";                //OBTENIENDO LA POSICION DE "+v+" \n";
                            if(r.hijos.get(0).getEtiqueta().equalsIgnoreCase("new")){
                                result =traducirObj(r,func);
                            }else{
                                result = operacionesArit(r.hijos.get(1),func);
                            }
                            
                            c3 += "Stack["+t + "] = "+result + ";                //ASIGNANDO VALOR A "+v+"\n";
                        }                        
                    }
                }else if(lbl.equalsIgnoreCase("==") || lbl.equalsIgnoreCase("===") || lbl.equalsIgnoreCase("!=") || lbl.equalsIgnoreCase("<>") || lbl.equalsIgnoreCase("!==")|| lbl.equalsIgnoreCase("<")|| lbl.equalsIgnoreCase(">")|| lbl.equalsIgnoreCase("<=")|| lbl.equalsIgnoreCase(">=")){
                    operacionesRel(r,func);                    
                }else if(lbl.equalsIgnoreCase("and") || lbl.equalsIgnoreCase("or") || lbl.equalsIgnoreCase("xor")){
                    operacionesBool(r,func);
                     c3 += "\n VERADEROS - "+globalEtiqV.substring(0, globalEtiqV.length()-1) + ":                  //CONDICION DE VERDADERO\n";
                     c3 += "\n FALSOS - "+globalEtiqF.substring(0, globalEtiqF.length()-1) + ":                  //CONDICION DE FALSO\n";
                }else if(lbl.equalsIgnoreCase("if")){
                    traducirIf(r,func); 
                }else if(lbl.equalsIgnoreCase("elseif") ){
                    c3 += "\n" + globalEtiqF.substring(0,globalEtiqF.length() -1) + ":               //NO SE CUMPLE EL IF, VERIFICA CON OTRO IF\n";
                    globalEtiqV = "";
                    globalEtiqF = "";
                    traducirIf(r,func);
                    //c3 += "\n \n" + globalEtiqF + ": \n";
                }else if(lbl.equalsIgnoreCase("else")){
                    c3 += "\n \n" + globalEtiqF.substring(0,globalEtiqF.length() -1) + ":              //NO SE CUMPLE EL IF\n";
                    TraduccionMetodo(r.hijos.get(0),func);
                }else if(lbl.equalsIgnoreCase("while")){
                    String etiqInic = getEtiq();
                    c3 += "\n" + etiqInic + ":         //ETIQUETA INICIAL DEL WHILE \n";
                    traducirWhile(r,func,etiqInic);
                }else if(lbl.equalsIgnoreCase("do")){
                    String etiqInic = getEtiq();
                    c3 += "\n" + etiqInic + ":         //ETIQUETA INICIAL DEL DO WHILE \n";
                    traducirDoWhile(r,func,etiqInic);                            
                }else if(lbl.equalsIgnoreCase("for")){
                    String etiqInic = getEtiq();
                    
                    nodo temp = r.hijos.get(0).hijos.get(0);
                   // JOptionPane.showMessageDialog(null,"NODO INICIAL: "+temp.hijos.get(0).getEtiqueta());
                    simbolo aux = this.tabla.getSim(temp.hijos.get(0).getEtiqueta(), func);                    
                    String t = getTemp();
                    String pos = "p +"+ aux.posicion;
                    c3 += t + " = " + pos + ";                //OBTENIENDO LA POSICION DE "+aux.nombre+"\n";
                   
                    c3 += "Stack["+t+"] = "+temp.hijos.get(1).getEtiqueta()+";                //ASIGNANDO VALOR INICIAL A "+aux.nombre+"\n";

                    c3 += "\n" + etiqInic + ":          //ETIQUETA INICIAL DEL FOR \n";
                    traducirFor(r,func,etiqInic);
                    
                    String t2 = getTemp();
                    String pos2 = "p +"+ aux.posicion;
                    c3 += t2 + " = " + pos2 + ";                //OBTENIENDO LA POSICION DE "+aux.nombre+"\n";
                    String t3 = getTemp();
                    c3 +=  t3 + " = stack["+t2+"];                      //OBTENIENDO EL VALOR DE "+aux.nombre+"\n";
                    
                    c3 += "stack["+t2+"] = "+t3+" + 1"+";                //ASIGNANDO AUMENTO A "+aux.nombre+"\n";
                    c3 += "go to "+etiqInic+"; \n";
                    
                    c3 += "\n" + globalEtiqF.substring(0,globalEtiqF.length() - 1)+":               //SALIDA DEL FOR\n";
                }else if(lbl.equalsIgnoreCase("switch")){
                    String etiqVerif = getEtiq();
                    c3 += "\n go to "+etiqVerif + ";                     //SWITCH SALVAJE APARECE\n\n";
                    traducirSwitch(r,func,etiqVerif);
                    
                }else if(lbl.equalsIgnoreCase("return")){//SI HAY UN RETURN
                    
                }else{//SI ES UN NODO NORMAL
                    this.TraduccionMetodo(r,func);
                }
            
                            
            }
        return c3;
    }
     
    //METODO PARA TRADUCIR A C3D LAS OPERACIONES ARITMETICAS
    public String operacionesArit(nodo raiz, String func){//LE PASO LA FUNCION PARA ENCONTRAR EL SIMBOLO
        
        String op1 ="";
        String op2 = "";
        // ES UNA OPERACION
        if(raiz.getEtiqueta().equalsIgnoreCase("+")|| raiz.getEtiqueta().equalsIgnoreCase("-") || raiz.getEtiqueta().equalsIgnoreCase("*") || raiz.getEtiqueta().equalsIgnoreCase("/")){ 
            String operador = raiz.getEtiqueta();
            if(raiz.hijos.get(0).getEtiqueta().equalsIgnoreCase("var")){
                op1 = operacionesArit(raiz.hijos.get(0).hijos.get(0),func);
            }else{
                op1 = operacionesArit(raiz.hijos.get(0),func);
            }
            
            if(raiz.hijos.get(1).getEtiqueta().equalsIgnoreCase("var")){
                op2 = operacionesArit(raiz.hijos.get(1).hijos.get(0),func);
            }else{
                op2 = operacionesArit(raiz.hijos.get(1),func);
            }
            
            String t =  getTemp();
            c3 += t + " = " + op1 + " "+ operador+" " + op2+"                //RESOLVIENDO LA OPERACION \n";
            return t;
        }else{
            if(raiz.getEtiqueta().equalsIgnoreCase("var")){
                raiz = raiz.hijos.get(0);
            }
            simbolo aux = this.tabla.getSim(raiz.getEtiqueta(), func);
            if(aux == null){//ES UN VALOR
                String t = getTemp();
                c3 += t + " = "+raiz.getEtiqueta() + ";                //ASIGNANDOLE EL VALOR AL NUEVO TEMPORAL\n";
                return t;
            }else{ //ES UNA VARIABLE
                String t = getTemp();
                String pos = "p +"+ aux.posicion;
                c3 += t + " = " + pos + ";                //OBTENIENDO LA POSICION DE "+aux.nombre+"\n";
                String ret = getTemp();
                c3 += ret + " = Stack["+t+"];                //OBTENIENDO EL VALOR DE "+aux.nombre+"\n";
                return ret;
            }
        }
    }
    
    //METODO PARA TRADUCIR A C3D LAS OPERACIONES RELACIONALES
    public void operacionesRel(nodo r, String func){
        String izq;
        String der;
        String lblV;
        String lblF;
        
        if(r.hijos.get(0).getEtiqueta().equalsIgnoreCase("var")){
            izq = operacionesArit(r.hijos.get(0).hijos.get(0),func);
        }else{
            izq = operacionesArit(r.hijos.get(0),func);
        }
        
        if(r.hijos.get(1).getEtiqueta().equalsIgnoreCase("var")){
            der = operacionesArit(r.hijos.get(1).hijos.get(0),func);
        }else{
            der = operacionesArit(r.hijos.get(1),func);
        }                    
        lblV = getEtiq();
        lblF = getEtiq();                    
        c3 += "if " + izq + " "+ r.getEtiqueta() + " "+der + " go to " + lblV + " \n"+ "go to "+lblF+"\n";

        globalEtiqV += lblV+",";
        globalEtiqF += lblF+",";        
    }
    
    //METODO PARA TRADUCIR A C3D LAS OPERACIONES BOOLEANAS
    public void operacionesBool(nodo raiz, String func){
        //JOptionPane.showMessageDialog(null,"nodo actual: "+raiz.getEtiqueta());
        String r = raiz.getEtiqueta();
        String lbl = raiz.hijos.get(0).getEtiqueta();
        if(lbl.equalsIgnoreCase("and") || lbl.equalsIgnoreCase("or") || lbl.equalsIgnoreCase("xor") || lbl.equalsIgnoreCase("not")){
            operacionesBool(raiz.hijos.get(0),func);
        }else{
            operacionesRel(raiz.hijos.get(0),func);
        }                        
        
        if(r.equalsIgnoreCase("or") || r.equalsIgnoreCase("||")){            
            c3 += "\n"+globalEtiqF.substring(0, globalEtiqF.length()-1) + ":                  //CONDICION DE FALSO DEL OR\n";
            globalEtiqF = "";
        }else if(r.equalsIgnoreCase("and") || r.equalsIgnoreCase("&&")){
            c3 += "\n" + globalEtiqV.substring(0, globalEtiqV.length()-1)+ ":                  //CONDICION DE VERDADERO DEL AND\n";
            globalEtiqV = "";
        }else if(r.equalsIgnoreCase("xor")){
            c3 += "\n"+globalEtiqF.substring(0,globalEtiqF.length()-1)+":                  //CONDICION DEL X FALSO DEL XOR\n"; 
            traducirXor(raiz.hijos.get(1),func);
        }else if(r.equalsIgnoreCase("not")){
            String aux;
            aux = globalEtiqV;
            globalEtiqV = globalEtiqF;
            globalEtiqF = aux;
            
            c3+= "VEDADEROS: "+ globalEtiqV.substring(0,globalEtiqV.length()-1)+"           //CAMBIO DEBIDO AL NOT\n";
            c3+= "FALSOS: "+ globalEtiqF.substring(0,globalEtiqF.length()-1)+"\n";
        }       

        if(r.equalsIgnoreCase("xor") || r.equalsIgnoreCase("not")){
            
        }else{
            if(raiz.hijos.get(1).getEtiqueta().equalsIgnoreCase("and") || raiz.hijos.get(1).getEtiqueta().equalsIgnoreCase("or")){
                operacionesBool(raiz.hijos.get(1),func);
            }else{
                operacionesRel(raiz.hijos.get(1),func);
            } 
        }
        
    }
    
    public void traducirXor(nodo condy, String func){//RECIBE TAMBIEN LAS ETIQUETAS VERDADERAS Y FALSAS
        String izq;
        String der;
        String lblV;
        String lblF;
        
        
        if(condy.hijos.get(0).getEtiqueta().equalsIgnoreCase("var")){
            izq = operacionesArit(condy.hijos.get(0).hijos.get(0),func);
        }else{
            izq = operacionesArit(condy.hijos.get(0),func);
        }
        if(condy.hijos.get(1).getEtiqueta().equalsIgnoreCase("var")){
            der = operacionesArit(condy.hijos.get(1).hijos.get(0),func);
        }else{
            der = operacionesArit(condy.hijos.get(1),func);
        }                    
        lblV = getEtiq();
        lblF = getEtiq();                    
        c3 += "if " + izq + " "+ condy.getEtiqueta() + " "+der + " go to " + lblV + " \n"+ "go to "+lblF+"\n";
        
        c3 += "\n"+globalEtiqV.substring(0,globalEtiqV.length()-1)+":                  //CONDICION DE X VERDADERO DEL XOR\n";
        
        
        if(condy.hijos.get(0).getEtiqueta().equalsIgnoreCase("var")){
            izq = operacionesArit(condy.hijos.get(0).hijos.get(0),func);
        }else{
            izq = operacionesArit(condy.hijos.get(0),func);
        }
        if(condy.hijos.get(1).getEtiqueta().equalsIgnoreCase("var")){
            der = operacionesArit(condy.hijos.get(1).hijos.get(0),func);
        }else{
            der = operacionesArit(condy.hijos.get(1),func);
        }                    
        c3 += "if " + izq + " "+ condy.getEtiqueta() + " "+der + " go to " + lblF+ " \n"+ "go to "+lblV+"\n";

        globalEtiqV = lblV+",";
        globalEtiqF = lblF+",";
    }
    
    public void traducirIf(nodo raiz, String func){
        nodo aux = raiz.hijos.get(0).hijos.get(0);
        String lbl = aux.getEtiqueta();
        if(lbl.equalsIgnoreCase("and") || lbl.equalsIgnoreCase("or") || lbl.equalsIgnoreCase("xor")|| lbl.equalsIgnoreCase("not") ){
            operacionesBool(aux,func);
        }else{
            operacionesRel(aux,func);
        } 
        c3 += "\n"+globalEtiqV.substring(0,globalEtiqV.length()-1)+":                  //SE CUMPLE EL IF\n";
        TraduccionMetodo(raiz.hijos.get(1), func);
    }   
    
    public String traducirObj(nodo raiz, String func){ //RECIBO EL NODO DONDE ESTA NEW
        String t = getTemp();
        String t2;
        String t3;
        c3 += t + " = h;\n";
        simbolo aux;
        aux = this.tabla.getSim(raiz.hijos.get(0).hijos.get(0).getEtiqueta(), func);
        simbolo aux2;
        c3 += "h = h + " + aux.tamano + ";\n";
        JOptionPane.showMessageDialog(null," pertenece a la funcion: "+func);
        
//        aux2 = this.tabla.getSim(func, func);
//        
//        c3 += "p = p + " + aux2.tamano + ";\n"; 
        t2 = getTemp();
        c3 += t2 + " = " + t + ";\n"; 
        t3 = getTemp();
        c3 += t3 + " = p + 0;\n";
        c3 += "stack["+t3+"] = "+t2+";\n";
        c3 += raiz.hijos.get(0).getEtiqueta()+"_construct();\n";
        c3 += "p = p - " + aux.tamano + "; \n";
        return t;
        //LLAMA AL CONSTRUCTOR
        //
        
    }
    
    public void traducirWhile(nodo raiz, String func, String etiqIni){
        nodo aux = raiz.hijos.get(0).hijos.get(0);
        String lbl = aux.getEtiqueta();
        if(lbl.equalsIgnoreCase("and") || lbl.equalsIgnoreCase("or") || lbl.equalsIgnoreCase("xor")|| lbl.equalsIgnoreCase("not") ){
            operacionesBool(aux,func);
        }else{
            operacionesRel(aux,func);
        } 
        c3 += "\n"+globalEtiqV.substring(0,globalEtiqV.length()-1)+":                  //SE CUMPLE LA CONDICION\n";
        TraduccionMetodo(raiz.hijos.get(1), func);
        c3 += "go to "+etiqIni + "; \n";
        c3 += "\n" + globalEtiqF.substring(0,globalEtiqF.length()-1)+":                 //SALIDA DEL DO WHILE\n";
 
    }
        
    public void traducirDoWhile(nodo raiz, String func, String etiqIni){
        nodo aux = raiz.hijos.get(1).hijos.get(0);
        String lbl = raiz.hijos.get(1).hijos.get(0).getEtiqueta();
        
        //JOptionPane.showMessageDialog(null,"NODO: "+aux.getEtiqueta());
        TraduccionMetodo(raiz.hijos.get(0), func);
        
        if(lbl.equalsIgnoreCase("and") || lbl.equalsIgnoreCase("or") || lbl.equalsIgnoreCase("xor")|| lbl.equalsIgnoreCase("not") ){
            operacionesBool(aux,func);
        }else{
            operacionesRel(aux,func);
        } 
        c3 += "\n"+globalEtiqV.substring(0,globalEtiqV.length()-1)+":                  //SE CUMPLE LA CONDICION\n";
        
        c3 += "go to "+etiqIni + "; \n";
        
        c3 += "\n" + globalEtiqF.substring(0,globalEtiqF.length()-1)+":                 //SALIDA DEL DO WHILE\n";
    }

    public void traducirFor(nodo raiz, String func, String etiqIni){
        nodo t = raiz.hijos.get(0).hijos.get(1);
        operacionesRel(t.hijos.get(0),func);
        
        c3 += "\n"+globalEtiqV.substring(0,globalEtiqV.length()-1)+":                  //SE CUMPLE LA CONDICION DEL FOR\n";
        TraduccionMetodo(raiz.hijos.get(1), func);
    }

    public void traducirSwitch(nodo raiz, String func, String etiq){
        ArrayList etiqPend = new ArrayList();
        ArrayList casePend = new ArrayList();
        nodo aux = raiz.hijos.get(1);
        String salida = getEtiq();
        for(int i = 0; i< aux.hijos.size(); i++){
            String lbl= getEtiq();
            etiqPend.add(lbl);
            
            c3 += lbl+":                   //ETIQUETA DE CASE DEL SWITCH \n";
            nodo aux2 = aux.hijos.get(i);
            casePend.add(aux2.getEtiqueta());
                if(aux2.hijos.get(aux2.hijos.size()-1).getEtiqueta().equalsIgnoreCase("break")){
                    TraduccionMetodo(aux2,func);
                    c3 += "\n go to " + salida+";              //ETIQUETA A LA SALIDA DEL SWITCH\n\n"; 
                }else{ 
                    TraduccionMetodo(aux2, func);
            }            
        }
        
        c3 += "\n"+etiq + ":             //VERIFICACION DEL CASE DEL SWITCH\n";
        
        String s = raiz.hijos.get(0).getEtiqueta();
        JOptionPane.showMessageDialog(null,"Nodo act:" + s.substring(1,s.length()));
        simbolo au = this.tabla.getSim(s.substring(1,s.length()), func);
        
        String t = getTemp();
        String pos = "p +"+ au.posicion;
        c3 += t + " = " + pos + ";                //OBTENIENDO LA POSICION DE "+au.nombre+"\n";
        String t2 = getTemp();
        c3 += t2 +" = stack["+t+"];             //OBTENIENDO EL VALOR DE "+au.nombre+"\n";
                
        for(int i = 0; i<etiqPend.size(); i++){
            c3 += "if "+t2+" = "+casePend.get(i) + " go to "+ etiqPend.get(i)+";\n";
        }
        
        c3 += "\n"+salida + ":               //ETIQUETA DE SALIDA DEL SWITCH"; 
                
    }
    
    public static String getTemp(){       
        ++generacion3d.temp;
        return "t"+generacion3d.temp;
    }
    
    public static String getEtiq(){
        ++generacion3d.etiqueta;
        return "L"+generacion3d.etiqueta;
    }
    
}
