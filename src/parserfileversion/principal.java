package parserfileversion;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import compilador.*;
import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author fg250
 */
public class principal extends javax.swing.JFrame {

    public principal() {
        initComponents();
        cantidadLineas(this.jTextPane3, this.jTextPane2, this.jScrollPane6);

    }
  
public void cantidadLineas(final JTextPane texto, final JTextPane linea, JScrollPane scroll) {

        texto.getDocument().addDocumentListener(new DocumentListener() {
            public String getText() {
                int posicion = texto.getDocument().getLength();
                javax.swing.text.Element ruta = texto.getDocument().getDefaultRootElement();
                String text = "1" + System.getProperty("line.separator");
                for (int i = 2; i < ruta.getElementIndex(posicion) + 2; i++) {
                    text += i + System.getProperty("line.separator");
                }
                return text;
            }
            
            @Override
            public void changedUpdate(DocumentEvent de) {
                linea.setText(getText());
            }

            @Override
            public void insertUpdate(DocumentEvent de) {
                linea.setText(getText());
            }

            @Override
            public void removeUpdate(DocumentEvent de) {
                linea.setText(getText());
            }
        });
        scroll.getViewport().add(texto);
        scroll.setRowHeaderView(linea);

    }
 
int aux = 1;
int incremento()
    {
        return aux++;
    }

    String recorrido(nodo raiz,int id){
            int var;            
            String cuerpo="";
            cuerpo += "nodo" + id + "[label=\"" + raiz.getEtiqueta() + "\", fillcolor=\"gray\", style =\"filled\", shape=\"box\"];\n";
            for (nodo hijos : raiz.hijos) { 
                var = incremento();
                cuerpo += "\"nodo"+ id + "\"->\"nodo"+var+"\"\n";
                cuerpo += recorrido(hijos, var);            
            }
          return cuerpo; 
    }
    
    public void Graficar(String cadena,String cad){
        FileWriter fichero = null;
        PrintWriter pw = null;
        String nombre=cad;
        String archivo=nombre+".dot";
        try {
            fichero = new FileWriter(archivo);
            pw = new PrintWriter(fichero);
            pw.println("digraph G {node[shape=box, style=filled, color=Gray95]; edge[color=blue];rankdir=UD \n");
            pw.println(cadena);
            pw.println("\n}");
            fichero.close();
          //  generarGrafico();
        } catch (Exception e) {
            System.out.println(e);
        } 
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane2 = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTextPane3 = new javax.swing.JTextPane();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTextPane2 = new javax.swing.JTextPane();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextPane4 = new javax.swing.JTextPane();
        jPanel2 = new javax.swing.JPanel();
        jTextPane5 = new javax.swing.JTextPane();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jTextArea2.setColumns(20);
        jTextArea2.setRows(5);
        jScrollPane2.setViewportView(jTextArea2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jSeparator1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jLabel1.setText("jLabel1");

        jScrollPane6.setViewportView(jTextPane3);

        jTextPane2.setForeground(new java.awt.Color(0, 102, 153));
        jTextPane2.setEnabled(false);
        jScrollPane7.setViewportView(jTextPane2);

        jLabel2.setForeground(new java.awt.Color(255, 0, 0));
        jLabel2.setToolTipText("");

        jPanel4.setAutoscrolls(true);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 402, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 457, Short.MAX_VALUE)
        );

        jPanel5.setAutoscrolls(true);

        jScrollPane4.setViewportView(jTextPane4);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jLabel2)
                .addGap(739, 739, 739))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(28, 28, 28))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(189, 189, 189)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(190, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(232, 232, 232)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(232, 232, 232)))
        );

        jTabbedPane1.addTab("Codigo Alto Nivel", jPanel1);

        jPanel3.setAutoscrolls(true);

        jScrollPane3.setViewportView(jTextPane1);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 23, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(344, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(jTextPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(428, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 449, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(24, 24, 24)))
        );

        jTabbedPane1.addTab("Errores", jPanel2);

        jMenu1.setText("Archivo");

        jMenuItem3.setText("Nuevo");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem4.setText("Abrir");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jMenuItem5.setText("Guardar");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        jMenuItem6.setText("Guardar como");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem6);

        jMenuItem7.setText("Salir");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem7);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Analisis");

        jMenuItem1.setText("Ejecutar analisis");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuItem2.setText("Mostrar errores");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Ayuda");

        jMenuItem8.setText("Manual tecnico");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem8);

        jMenuItem9.setText("Manual de usuario");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem9);

        jMenuItem10.setText("Acerca de");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem10);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
                        .addGap(432, 432, 432))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 505, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    tabla_simbolos ts; //DECLARACION DE LA TABLA DE SIMBOLOS
    String Texto="";
    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        analisis parser = new analisis(new java.io.StringReader(this.jTextPane3.getText()));
        ts  = new tabla_simbolos();
        try {
           nodo raiz= parser.S();
           Graficar(recorrido(raiz ,0),"arbol"); 
          
           recorridoArbol(raiz);
           ts.imprimir();
           System.out.println("Exito!!"); 
           generacion3d g = new generacion3d(ts,raiz);
           Texto += g.c3;
           JOptionPane.showMessageDialog(this,Texto);
           this.jTextPane4.setText(Texto);
        }catch (ParseException | HeadlessException  e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
            System.out.println(e.getMessage());
        }
       
    }//GEN-LAST:event_jMenuItem1ActionPerformed
   
    nodo tmp;
    
    public void LeerInclude(String archivo){
        String cadena2="";
        
        
        try {
            cadena2 = leerArch(archivo);
        } catch (IOException ex) {
            Logger.getLogger(principal.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.jTextPane5.setText(cadena2);
        analisis parser = new analisis(new java.io.StringReader(cadena2));
        try {
           nodo r= parser.S();
        //   Graficar(recorrido(r ,0),"arbol"); 
           recorridoArbol(r);
           ts.imprimir();
           System.out.println("Exito!!"); 
           generacion3d g2 = new generacion3d(ts,r);
           Texto += g2.c3;
           //JOptionPane.showMessageDialog(this,g2.c3);
           //this.jTextPane4.setText(g2.c3);
        }catch (ParseException | HeadlessException  e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
            System.out.println(e.getMessage());
        }
    }
    
    public String leerArch(String archivo) throws FileNotFoundException, IOException{
          String cadena;
        String cadena2="";
        FileReader f= new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        while((cadena = b.readLine())!=null) {
            cadena2 += cadena+"\n";
            System.out.println(cadena);
        }
        b.close();
        
        return cadena2;
    }
    
    public void recorridoArbol(nodo raiz){
        //CONDICION PARA QUE OBTENGA LA CLASE
        
        if(raiz.getEtiqueta().equalsIgnoreCase("raiz")){
            nodo t = raiz.hijos.get(0);
            if(!t.hijos.isEmpty()){
                for(int i = 0; i<t.hijos.size(); i++){
                    JOptionPane.showMessageDialog(this,"SE ENCONTRO UN INCLUDE DEL ARCHIVO"+t.hijos.get(i).getEtiqueta());
                    LeerInclude(t.hijos.get(i).getEtiqueta());                
                }
            }
            
            recorridoArbol(raiz.hijos.get(1));
        }
        if(raiz.getEtiqueta().equalsIgnoreCase("CLASES")){ 
            if(!raiz.hijos.isEmpty()){
                int tamClase = 0;
                
                //APUNTANDO AL NODO QUE CONTIENE EL NOMBRE DE LA CLASE
                raiz = raiz.hijos.get(0);  
                String nom=raiz.getEtiqueta();
                System.out.println("NOMBRE DE LA CLASE: "+nom); 
                int herencia;
                
                //REVISANDO SI LA CLASE TIENE HERENCIA O NO
                if(raiz.hijos.get(1).getEtiqueta().equalsIgnoreCase("EXTENDS")){
                    herencia = 1;                    
                }else{
                    herencia = -1;
                }
                nodo temp = raiz.hijos.get(0);
                
                //DETECTANDO LAS VARIABLES GLOBALES DE LA CLASE
                for(int i =0; i< temp.hijos.size(); i++){   
                    String nomAt= temp.hijos.get(i).getEtiqueta(); 
                    System.out.println("VARIABLE NOMBRE: " + nomAt);                    
                    simbolo nuevo = new simbolo(nomAt,"pend",1,i,"publico",nom,"atributo",-1,-1,-1);                                        
                    ts.add(nuevo);                    
                    tamClase++;
                }     
                
                //OBTENIENDO LAS FUNCIONES DE LA CLASE
                for(int j=1; j<raiz.hijos.size(); j++){ 
                    nodo aux = raiz.hijos.get(j);
                    
                    recorridoFuncion(aux, nom); //LE ENVIO LA RAIZ DE LA FUNCION PARA QUE LO ANALICE Y EL NOMBRE DE LA CLASE PARA QUE SEPA DONDE PERTENECE
                }
                
                //CREANDO EL SIMBOLO PARA LA CLASE Q SE INSERTARA EN LA TABLA DE SIMBOLOS
                simbolo nclase = new simbolo(nom, "clase",tamClase,-1,"publico","global","clase",-1,-1,herencia);                
                ts.add(nclase);
                
                System.out.println("TAMANO DE LA CLASE: "+tamClase);
                
            }else{// SI NO TIENE CLASES DEFINIDAS, ENTONCES ES EL ARCHIVO MAIN
                System.out.println("ESTE ES EL ARCHIVO MAIN");
                //JOptionPane.showMessageDialog(this,"NODO ACTUAL: "+raiz.getEtiqueta());
                recorridoFuncion(tmp, "main");
            }            
       }else if(raiz.getEtiqueta().equalsIgnoreCase("EXPRESIONES")){
           tmp = raiz;
       }else if(!raiz.hijos.isEmpty()){ //CONTINUA MOVIENDOSE ENTRE LOS NODOS
             for(int i =0; i< raiz.hijos.size(); i++){                              
               recorridoArbol(raiz.hijos.get(i));
           }
        }
    }
    
    int posicion;
    public void recorridoFuncion(nodo raiz, String clase){
        String nom;
        String tipo ="";
        int noParam=0;
        int pos = 0; 
        int tam=0;
        
        if(clase.equalsIgnoreCase("main")){
            nom = clase;
            tipo = clase;
        }else{
            //OBTIENE EL NUMERO DE PARAMETROS QUE POSEE EL METODO      
            noParam = raiz.hijos.get(1).hijos.size();  

            //DETERMINA SI LA FUNCION ES CONSTRUCTOR O UNA FUNCION NORMAL
            if(raiz.getEtiqueta().equalsIgnoreCase("CONSTRUCTOR")){
                nom = clase+ raiz.hijos.get(0).getEtiqueta();
                System.out.println("ESTE ES EL METODO CONSTRUCTOR Y SE LLAMARA: " + nom + " Y TIENE: "+noParam + " PARAMETROS" ); 
                tipo="constructor";
            }else{
                nom = raiz.hijos.get(0).getEtiqueta() + "__" + noParam;
                System.out.println("ESTE ES UN METODO Y SE LLAMARA: " + nom + " Y TIENE: "+noParam + " PARAMETROS" );
                tipo = "funcion";
            }
        }
        //CREANDO EL SIMBOLO PARA EL THIS
        simbolo nsim = new simbolo("this",clase,1,0,"-1",nom,"this",-1,-1,-1);     
        ts.add(nsim);
        
        //CREANDO EL SIMBOLO PARA EL RETURN
        ts.add(new simbolo("return","pend",1,1,"-1",nom,"return",-1,-1,-1));
     
        nodo temp = raiz.hijos.get(1);
        pos =2;
        if(!clase.equalsIgnoreCase("main")){            
            //ANADIENDO LOS PARAMETROS A LA TABLA DE SIMBOLOS
            for(int i =0; i<temp.hijos.size(); i++){
                nodo n = temp.hijos.get(i).hijos.get(0);
                simbolo npar = new simbolo(n.getEtiqueta(),"pend",1,pos,"-1",nom,"parametro",-1,-1,-1);
                ts.add(npar);
                pos++;
            }
        }
        
        
        posicion = pos;
        
        //BUSCANDO NUEVAS VARIABLES ENTRE LAS FUNCIONES
        if(clase.equalsIgnoreCase("main")){
            recorridoAtributos(raiz,nom);
            tam=posicion;
            //CREANDO EL SIMBOLO PARA LA FUNCION
            simbolo nFunc = new simbolo(nom,"void",tam,-1,"publico","global",tipo,-1,noParam,-1);     
            ts.add(nFunc);
        }else{
            raiz = raiz.hijos.get(2);
            recorridoAtributos(raiz,nom);
            tam = posicion;
            //CREANDO EL SIMBOLO PARA LA FUNCION
            simbolo nFunc = new simbolo(nom,clase,tam,-1,"publico",clase,tipo,-1,noParam,-1);     
            ts.add(nFunc);
        }
        
    }
    
    public void recorridoAtributos(nodo raiz, String func){
        for (nodo r : raiz.hijos) { 
                if(r.getEtiqueta().equalsIgnoreCase("var")){
                    String v =r.hijos.get(0).getEtiqueta();
                    if(!ts.simExist(v, func)){
                        simbolo nvar = new simbolo(v,"pend",1,posicion,"-1",func,"variable",-1,-1,-1);
                        ts.add(nvar);
                        posicion++;
                    }
                }else if(r.getEtiqueta().equalsIgnoreCase("=")){
                    if(r.hijos.get(0).getEtiqueta().equalsIgnoreCase("var")){
                        String v =r.hijos.get(0).hijos.get(0).getEtiqueta();
                        if(!ts.simExist(v, func)){
                           simbolo nvar = new simbolo(v,"pend",1,posicion,"-1",func,"variable",-1,-1,-1);
                           ts.add(nvar);
                           posicion++;
                        }
                    }
                }else if(r.getEtiqueta().equalsIgnoreCase("return")){
                    
                }else{
                    recorridoAtributos(r, func);
                }
            
                            
            }
        
    }
    
    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
   
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
     
        System.exit(0);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed

    }//GEN-LAST:event_jMenuItem3ActionPerformed
    
    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
       
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed

        JOptionPane.showMessageDialog(this, "ARCHIVOS GUARDADOS EXITOSAMENTE!");
     
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed

    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        
        JOptionPane.showMessageDialog(this, "Desarrollado por: Felix Alberto Garcia C.");
        JOptionPane.showMessageDialog(this, "Carnet No. 200915675");
        JOptionPane.showMessageDialog(this, "Laboratorio de Compiladores 2");
        
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
       
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
      
    }//GEN-LAST:event_jMenuItem8ActionPerformed
       
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new principal().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JTextPane jTextPane2;
    private javax.swing.JTextPane jTextPane3;
    private javax.swing.JTextPane jTextPane4;
    private javax.swing.JTextPane jTextPane5;
    // End of variables declaration//GEN-END:variables
	  
}//principal
